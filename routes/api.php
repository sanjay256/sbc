<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::post('login', 'API\UserAPIController@login');
/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
	
}); */
/*Route::middleware('auth:api')->group(function () {
	Route::get('user', 'API\UserAPIController@user');
	Route::get('alldata', 'API\UserAPIController@alldata');
	Route::get('logout', 'API\UserAPIController@logout');
}); */



/* JWT Auth */
Route::group(['prefix' => 'v1'], function () {
	Route::group(['namespace' => 'Auth'], function () {
        Route::post('auth', ['as' => 'login', 'uses' => 'AuthController@login']);
		Route::post('register', ['as' => 'register', 'uses' => 'AuthController@register']);
		Route::get('status', ['as' => 'status.index', 'uses' => 'StatusController@index']);
	});
	
	Route::group(['namespace' => 'API'], function () {
		Route::group(['namespace' => 'Status'], function () {		
		Route::get('status', ['as' => 'status.index', 'uses' => 'StatusController@index']);
		});
	});
	
    Route::group(['middleware' => [ 'jwt', 'jwt.auth']], function () {
		
        Route::group(['namespace' => 'API'], function () {
			
		 Route::group(['namespace' => 'Status'], function () {	
		 
            /* Route::get('status', ['as' => 'status.index', 'uses' => 'StatusController@index']); */

            Route::post('status', ['as' => 'status.store', 'uses' => 'StatusController@store']);

            Route::get('status/{id}', ['as' => 'status.show', 'uses' => 'StatusController@show']);

            Route::put('status/{id}', ['as' => 'status.update', 'uses' => 'StatusController@update']);

            Route::delete('status/{id}', ['as' => 'status.destroy', 'uses' => 'StatusController@destroy']);

		 });
		 
        });
    });  
});
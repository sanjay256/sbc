<?php
/**
 * File name: UserAPIController.php
 * Last modified: 2020.06.11 at 12:09:19
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 */

namespace App\Http\Controllers\API\Status;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class StatusController extends Controller
{
    public function __construct() {
    
	}
	
	public function index(Request $request) 
    {
			$status_type = 'Public'; 	
			if (!is_null($request->bearerToken())) {
			   $user = JWTAuth::parseToken()->authenticate();
			   $status_type = $status_type = 'Private';
			}
		
			
		   $AllStatus = Status::select('name','slug','status_type');
		   
		   $expiration = date("Y-m-d H:i:s");
		   	$AllStatus = $AllStatus->where('expiration', '>=', "$expiration");
		   
		   if($request->search !=null)
			{
				$search  = $request->search;
				$AllStatus = $AllStatus->where('name', 'LIKE', "%$search%");
			}
			
			if($request->status_type !=null)
			{
				$searchstatus_type  = $request->status_type;
				$AllStatus = $AllStatus->where('status_type', '=', "$searchstatus_type");
			}
			$AllStatus = $AllStatus->where(function ($query) use ($status_type) {
					$query->where('status_type', '=', 'Public')
					->orWhere('status_type', '=', $status_type);
			});
			
			$AllStatus = $AllStatus->orderBy('id', 'asc');
			
         $limit = $request->limit;
		 
		$offset = $request->offset;
        if ($limit) {
            $AllStatus = $AllStatus->limit($limit);
        }

        if ($offset && $limit) {
            $AllStatus = $AllStatus->skip($offset);
        }
			/* $AllStatus = $AllStatus->paginate($request->limit); */
			$AllStatus = $AllStatus->get();
		
		return response(['data' => $AllStatus,'message'=>'Status data retrieved successfully'],200);
    }
	
	public function store(Request $request)
    {
        try {
			
			$validator = Validator::make($request->all(), [
                'name' => 'required',
				'status_type' => 'required',
                'expiration' => 'required|numeric',
            ]);

            if($validator->fails()){
				return response()->json(['error' => $validator->errors()], 400);
            }
			$expiration = $request->get('expiration');
			$expiration = date("Y-m-d H:i:s", strtotime("+$expiration hours"));
			
			$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->get('name'))));
			$slugCount = count(Status::whereRaw("(slug = '$slug' or slug LIKE '$slug-%')")->get());
			$slug = $slugCount == 0 ? $slug : $slug.'-'.$slugCount;			
			$user = JWTAuth::parseToken()->authenticate();
			$userId = $user->id;
			$status = Status::create([
				'name' => $request->get('name'),
				'status_type' => $request->get('status_type'),
				'slug' => $slug,
				'expiration' => $expiration,
				'user_id' => $userId,
			]);
			 return response()->json([
                'data' => $status,
                'message' => 'Status Post successfully'
            ]); 
        } catch (Illuminate\Database\QueryException $e) {
            
            return response()->json([
                'error' => 'user_cannot_create',
                'message' => $e->getMessage()
            ]);        
        }
    }
	
	public function show($id)
    {
        try {
			 $GetStatus = Status::select('name','slug','status_type');
			 $GetStatus = $GetStatus->where('slug', '=', "$id");
			 $GetStatus = $GetStatus->get();
			 
			return response(['data' => $GetStatus,'message'=>'Status data retrieved successfully'],200);
          
            
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            
            return response()->json([
                'error' => 'user_no_found',
                'message' => $e->getMessage()
            ]);
        }
    }
	public function update(Request $request, $id)
    {
        // do data validation
        
        try {
            
            $user = $this->userRepo->findOneOrFail($id);
            
            // Create an instance of the repository again 
            // but now pass the user object. 
            // You can DI the repo to the controller if you do not want this.
            $userRepo = new UserRepository($user);
            
            $userRepo->update($request->all());

            $data = $this->userRepo->transform($user)->toArray();

            return response()->json($data, 201);
            
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            
            return response()->json([
                'error' => 'user_no_found',
                'message' => $e->getMessage()
            ]);            
            
        } catch (Illuminate\Database\QueryException $e) {
            
            return response()->json([
                'error' => 'user_cannot_update',
                'message' => $e->getMessage()
            ]);
        }
    }
    
    public function destroy($id)
    {
        
        
        try {
			
			$GetStatus = Status::select('id','name','slug','status_type');
			$GetStatus = $GetStatus->where('slug', '=', "$id");
			$GetStatus = $GetStatus->first();
			if($GetStatus !=null)
			{
				$GetStatus->delete();
				return response(['data' => [],'message'=>"$GetStatus->name deleted successfully"],200);
			}
			
            return response(['data' => [],'message'=>"Status not found"],200);
			
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            
            return response()->json([
                'error' => 'user_no_found',
                'message' => $e->getMessage()
            ]);            
            
        } catch (Illuminate\Database\QueryException $e) {
            
            return response()->json([
                'error' => 'user_cannot_delete',
                'message' => $e->getMessage()
            ]);
        }
    }
	
}
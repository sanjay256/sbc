<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Hashing\BcryptHasher;
use JWTFactory;
use JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
	
	public $loginAfterSignUp = true;
	
	public function register(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
				'username' => 'required|string|max:255|unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);

            if($validator->fails()){
				return response()->json(['error' => $validator->errors()], 400);
            }

           
			$ip_address = $request->ip();	
			$user = User::create([
				'name' =>$request->get('name'),
				'username' =>$request->get('username'),
				'email' => $request->get('email'),
				'password' => Hash::make($request->get('password')),
				'api_token' => Str::random(60),
				'ip_address' => $ip_address,
			]);
		
            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'),201);
        }
		

    public function login(Request $request)
    {
        
		$validator = Validator::make($request->all(), [
			'emailorusername' => 'required',
			'password' => 'required',
		]);
	
		if ($validator->fails())
		{
			
			return response()->json(['error' => $validator->errors()], 400);
		}
		
		
			
        $user = User::where('email', $request->emailorusername)->orWhere('username',$request->emailorusername)->first();
		
        if(!$user) return response()->json(['error' => 'User not found.'], 404);

        // Account Validation
        if (!(new BcryptHasher)->check($request->input('password'), $user->password)) {

            return response()->json(['error' => 'Password is incorrect. Authentication failed.'], 401);
        }

        // Login Attempt
		if(filter_var($request->emailorusername, FILTER_VALIDATE_EMAIL)) {
			//user sent their email 
			$credentials = (['email' => $request->emailorusername, 'password' => $request->input('password')]);
		} else {
			//they sent their username instead 
			$credentials = (['username' => $request->emailorusername, 'password' => $request->input('password')]);
		}
		
		/**/
        try {
            // JWTAuth::factory()->setTTL(40320); // Expired Time 28days
            if (! $token = JWTAuth::attempt($credentials, ['exp' => Carbon::now()->addDays(28)->timestamp])) {
				
                return response()->json(['error' => 'invalid_credentials'], 401);

            }
			
        } catch (JWTException $e) {
				
            return response()->json(['error' => 'could_not_create_token'], 500);

        }
		$user->update([
        'last_login_at' => Carbon::now()->toDateTimeString(),
        'last_login_ip' => $ip_address = $request->ip()
    ]);
        return response()->json(compact('token', 'user'));

    }
}